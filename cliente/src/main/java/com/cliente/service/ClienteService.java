package com.cliente.service;

import com.cliente.model.Cliente;
import com.cliente.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteService {
    @Autowired
    private ClientRepository repository;

    @Transactional
    public Cliente save(Cliente cliente) {
        Cliente cliente1 = repository.save(cliente);
        return cliente1;
    }
    public List<Cliente> getClients() {

        return repository.findAll();
    }
    public Cliente getClientById(Integer id) {

        return repository.findById(id).get();
    }

}
