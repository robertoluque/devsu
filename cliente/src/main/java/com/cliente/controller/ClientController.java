package com.cliente.controller;

import com.cliente.model.Cliente;
import com.cliente.service.ClienteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/client")
public class ClientController {
    @Autowired
    private ClienteService clienteService;

    @GetMapping("/list")
    public List<Cliente> listClients()  {
        log.info("list Clients");
        return clienteService.getClients();
    }

    @PostMapping
    public void createClient(@RequestBody Cliente cliente)  {
        log.info("create client");
        clienteService.save(cliente);
    }

    @GetMapping("/{id}")
    public Cliente getCliente(@PathVariable("id") Integer id)  {
        log.info("list Clients");
        return clienteService.getClientById(id);
    }

}
