
CREATE TABLE IF NOT EXISTS cliente
(
    id       bigint auto_increment
        primary key,
    edad     int          null,
    genero   varchar(255) null,
    nombre   varchar(255) null,
    estado   varchar(255) null,
    password varchar(255) null
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- auto-generated definition
CREATE TABLE IF NOT EXISTS cuenta
(
    id              int auto_increment
        primary key,
    estado          varchar(255)   null,
    idcliente       int            null,
    nrocuenta       varchar(255)   null,
    saldodisponible decimal(38, 2) null,
    saldoinicial    decimal(38, 2) null,
    tipocuenta      varchar(255)   null
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS movimiento
(
    id             int auto_increment
        primary key,
    fecha          datetime(6)    null,
    idcuenta       int            null,
    saldo          decimal(38, 2) null,
    tipomovimiento varchar(255)   null,
    valor          decimal(38, 2) null
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
