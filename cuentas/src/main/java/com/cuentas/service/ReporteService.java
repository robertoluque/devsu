package com.cuentas.service;

import com.cuentas.dto.ClienteDto;
import com.cuentas.dto.RequestDtoReporte;
import com.cuentas.dto.ResponseDtoReporte;
import com.cuentas.model.Cuenta;
import com.cuentas.model.Movimiento;
import com.cuentas.repository.CuentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReporteService {
    @Autowired
    private MovimientoService movimientoService;
    @Autowired
    private CuentaService cuentaService;
    @Autowired
    private RestTemplate restTemplate;

    public List<ResponseDtoReporte> getMovimientos(RequestDtoReporte requestDtoReporte) {
        ResponseDtoReporte response = null;
        List<ResponseDtoReporte> responseList = new ArrayList<ResponseDtoReporte>();
        List<Cuenta> cuentas = cuentaService.getCuentasByCliente(requestDtoReporte.getCliente());


        for(Cuenta cuenta: cuentas){
//            List<Movimiento> movimientos= movimientoService.getMovimientosByCuenta(cuenta.getId());
            List<Movimiento> movimientos= movimientoService.getMovimientosByCuentaFecha(
                    cuenta.getId(),requestDtoReporte.getFechaDesde(),requestDtoReporte.getFechaHasta());

            ClienteDto clienteDto = restTemplate.getForObject("http://apicliente:8080/client/{id}", ClienteDto.class, cuenta.getIdCliente());

            for(Movimiento movimiento:movimientos) {
                response= new ResponseDtoReporte();
                response.setCliente(clienteDto.getNombre());
                response.setEstado(clienteDto.getEstado());
                response.setSaldo(movimiento.getSaldo());
                response.setValor(movimiento.getValor());
                response.setNroCuenta(cuenta.getNroCuenta());
                response.setFechaMov(movimiento.getFecha());
                response.setSaldoInicial(cuenta.getSaldoInicial());
                response.setTipoCuenta(cuenta.getTipoCuenta());
                responseList.add(response);
            }


        }
        return responseList;
    }

}
