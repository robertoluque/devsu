package com.cuentas.service;

import com.cuentas.exceptions.SaldoInsuficienteException;
import com.cuentas.model.Cuenta;
import com.cuentas.model.Movimiento;
import com.cuentas.repository.CuentaRepository;
import com.cuentas.repository.MovimientoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MovimientoService {
    @Autowired
    private MovimientoRepository repository;
    @Autowired
    private CuentaRepository cuentaRepository;

    @Transactional
    public Movimiento save(Movimiento movimiento) {
        Optional<Cuenta> cuenta = cuentaRepository.findById(movimiento.getIdCuenta());
        BigDecimal nuevoSaldo= BigDecimal.valueOf(0);
        BigDecimal saldoDisponible= cuenta.get().getSaldoDisponible();
        if(movimiento.getTipoMovimiento().equals("RET")) {
            if(movimiento.getValor().compareTo(saldoDisponible) == 1) {
                throw new SaldoInsuficienteException("Monto excede saldo disponible " + saldoDisponible);
            } else {
                nuevoSaldo= saldoDisponible.subtract(movimiento.getValor());
            }
        }else {
            nuevoSaldo=saldoDisponible.add(movimiento.getValor());
        }
        movimiento.setSaldo(nuevoSaldo);
        cuenta.get().setSaldoDisponible(nuevoSaldo);
        Movimiento mov = repository.save(movimiento);
        cuentaRepository.save(cuenta.get());
        return mov;
    }
    public List<Movimiento> getMovimientos() {

        return repository.findAll();
    }

    public List<Movimiento> getMovimientosByCuenta(Integer idCuenta) {
        Movimiento movimiento=new Movimiento();
        ExampleMatcher matcher = ExampleMatcher.matchingAny();
        movimiento.setIdCuenta(idCuenta);
        Example<Movimiento> example = Example.of(movimiento,matcher);
        return repository.findAll(example);
    }

    public List<Movimiento> getMovimientosByCuentaFecha(Integer idCuenta, Date fechaDesde, Date fechaHasta) {
        return  repository.findMovimientoByIdCuentaAndFechaBetween(idCuenta,fechaDesde,fechaHasta);
    }

}
