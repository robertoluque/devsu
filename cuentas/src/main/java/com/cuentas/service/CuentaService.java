package com.cuentas.service;

import com.cuentas.model.Cuenta;
import com.cuentas.model.Movimiento;
import com.cuentas.repository.CuentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;

@Service
public class CuentaService {
    @Autowired
    private CuentaRepository repository;

    @Transactional
    public Cuenta save(Cuenta cuenta) {
        cuenta.setSaldoDisponible(cuenta.getSaldoInicial());
        Cuenta cta = repository.save(cuenta);
        return cta;
    }
    public List<Cuenta> getCuentas() {
        return repository.findAll();
    }

    public Cuenta getCuentaById(Integer idCuenta) {
        return repository.findById(idCuenta).get();
    }

    public List<Cuenta> getCuentasByCliente(Integer idCliente) {
        Cuenta cuenta=new Cuenta();
        ExampleMatcher matcher = ExampleMatcher.matchingAny();
        cuenta.setIdCliente(idCliente);
        Example<Cuenta> example = Example.of(cuenta,matcher);
        return repository.findAll(example);
    }
}
