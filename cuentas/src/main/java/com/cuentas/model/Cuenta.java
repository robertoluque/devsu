package com.cuentas.model;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Entity
public class Cuenta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	@Column(name = "NROCUENTA")
	private String nroCuenta;
	@Column(name = "TIPOCUENTA")
	private String tipoCuenta;
	@Column(name = "SALDOINICIAL")
	private BigDecimal saldoInicial;
	private String estado;
	@Column(name = "IDCLIENTE")
	private Integer idCliente;
	@Column(name = "SALDODISPONIBLE")
	private BigDecimal saldoDisponible;


}
