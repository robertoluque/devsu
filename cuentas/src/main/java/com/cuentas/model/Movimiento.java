package com.cuentas.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
public class Movimiento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date fecha;
	@Column(name = "TIPOMOVIMIENTO")
	private String tipoMovimiento;
	private BigDecimal valor;
	private BigDecimal saldo;
	@Column(name = "IDCUENTA")
	private Integer idCuenta;

}
