package com.cuentas.repository;

import com.cuentas.model.Movimiento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MovimientoRepository extends JpaRepository<Movimiento, Integer> {
    public List<Movimiento> findMovimientoByIdCuentaAndFechaBetween(Integer idCuenta, Date startFecha, Date endFecha);
}
