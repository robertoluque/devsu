package com.cuentas.dto;

import lombok.Data;

@Data
public class ClienteDto {
    private Long id;
    private String password;
    private String estado;
    private String nombre;
    private String genero;
    private Integer edad;
}
