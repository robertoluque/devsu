package com.cuentas.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@Data
public class ResponseDtoReporte {
    private Date fechaMov;
    private String cliente;
    private String nroCuenta;
    private String tipoCuenta;
    private BigDecimal saldoInicial;
    private String estado;
    private BigDecimal valor;
    private BigDecimal saldo;
}
