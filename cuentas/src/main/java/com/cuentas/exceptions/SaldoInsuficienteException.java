package com.cuentas.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="Saldo insuficiente")
public class SaldoInsuficienteException extends  RuntimeException{
    public SaldoInsuficienteException(String msg) {
        super(msg);
    }
}
