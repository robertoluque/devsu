package com.cuentas.controller;

import com.cuentas.dto.RequestDtoReporte;
import com.cuentas.dto.ResponseDtoReporte;
import com.cuentas.model.Movimiento;
import com.cuentas.service.MovimientoService;
import com.cuentas.service.ReporteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/reporte")
public class ReporteController {
    @Autowired
    private ReporteService reporteService;

    @PostMapping
    public List<ResponseDtoReporte> listMovimientos(@RequestBody RequestDtoReporte requestDtoReporte)  {
        log.info("list movimientos");
        return reporteService.getMovimientos(requestDtoReporte);
    }


}
