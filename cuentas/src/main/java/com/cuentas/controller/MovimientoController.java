package com.cuentas.controller;

import com.cuentas.model.Cuenta;
import com.cuentas.model.Movimiento;
import com.cuentas.service.CuentaService;
import com.cuentas.service.MovimientoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/movimiento")
public class MovimientoController {
    @Autowired
    private MovimientoService movimientoService;

    @GetMapping
    public List<Movimiento> listMovimientos()  {
        log.info("list movimientos");
        return movimientoService.getMovimientos();
    }

    @PostMapping
    public ResponseEntity createMovimiento(@RequestBody Movimiento movimiento)  {
        log.info("create movimiento");
//        try{
            movimientoService.save(movimiento);
            return new ResponseEntity(HttpStatus.CREATED);
//        }catch (Exception ex){
//            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//        }
    }

}
