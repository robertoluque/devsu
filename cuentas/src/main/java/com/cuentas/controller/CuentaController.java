package com.cuentas.controller;

import com.cuentas.model.Cuenta;
import com.cuentas.service.CuentaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/cuentas")
public class CuentaController {
    @Autowired
    private CuentaService cuentaService;

    @GetMapping
    public List<Cuenta> listCuentas()  {
        log.info("list cuentas");
        return cuentaService.getCuentas();
    }

    @PostMapping
    public void createCuenta(@RequestBody Cuenta cuenta)  {
        log.info("create cuenta");
        cuentaService.save(cuenta);
    }

}
